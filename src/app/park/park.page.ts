import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { UtilsService } from '../services/utils.service';
import { ParkService, ParkingModel, PropertyModel } from '../services/park.service';
import { PopoverController } from '@ionic/angular';
import { VehicleSelectionPage } from '../vehicles/vehicle-selection/vehicle-selection.page';
import { VehicleModel, VehicleService } from '../services/vehicle.service';
import { PropertyService } from '../services/property.service';
import { VisitorService, VisitModel } from '../services/visitor.service';
import { CountdownComponent, CountdownConfig, CountdownEvent } from 'ngx-countdown';
import { VisitService } from '../services/visit.service';




@Component({
  selector: 'app-park',
  templateUrl: './park.page.html',
  styleUrls: ['./park.page.scss'],
})
export class ParkPage implements OnInit, OnDestroy {

  search: string = "";

  searchProperty: string = "";
  property: PropertyModel;
  properties: PropertyModel[] = [];

  park: ParkingModel = null;
  vehicleSelected: VehicleModel;

  parks: ParkingModel[] = [];

  @ViewChild('cd', { static: false }) private countdown: CountdownComponent;

  constructor(
    public utils: UtilsService,
    public service: ParkService,
    public popoverController: PopoverController,
    public propertyService: PropertyService,
    public vehicleService: VehicleService,
    public visitorService: VisitorService,
    public visitService: VisitService,

  ) { }

  ionViewDidEnter() {
    var that = this;
    document.addEventListener("backbutton",function(e) {
      // that.utils.presentAlert([{text:'ok'}], 'asdasd','','');
    }, false);
}

  handleEvent(counterEvent: CountdownEvent) {
    // console.log(counterEvent)
    if (counterEvent.action == 'done') {
      this.loadData();
    }
  }

  ngOnInit() {
    
    this.loadData();
  }

  ngOnDestroy(): void {
    
    this.visitService.visit = null;
  }



  async loadData() {
   
 
    // if(this.visitorService.visitor!=null)
    // {
    await this.utils.presentLoading();

    this.visitorService.GetVisitorLogged().subscribe(y => {
  
      
    

      this.visitService.getMyLastPark(this.visitorService.visitor.id).subscribe(x => {
        this.utils.dismissLoading();

        if (this.visitService.visit != null && !this.visitService.visit.wasCancelled && !this.visitService.visit.hasExpired) {
          setTimeout(() => {
            this.resetCounter(this.visitService.visit.seconds);
          }, 500);
        }
      }, () => { this.utils.dismissLoading(); });

    
    }, () => { this.utils.dismissLoading(); });
  // }
  }




  config: CountdownConfig;

  resetCounter(value) {
    this.config = { leftTime: value };
    this.countdown.begin();
  }



  searchPark() {

    // console.log(this.search)

    this.service.SearchByNumber(this.search).subscribe(async x => {
      if (x == null) {
        this.utils.presentToast('Location Code Not Found.', 'danger', 4000);
      }
      else {
        this.park = x;
        this.search = "";

        await this.utils.presentLoading();
        this.vehicleService.GetListByVisitor(this.visitorService.visitor.id).subscribe(async x => {
          this.utils.dismissLoading();

          this.carSelection();


        }, () => { this.utils.dismissLoading(); });

      }

    });

  }

  onSearch() {
    var temp = this.search;

    setTimeout(() => {
      if (temp == this.search) {
        this.service.Search(this.search).subscribe(x => {
          this.parks = x;
        });
      }
    }, 500);
  }

  onSelectLocation(item: ParkingModel) {
    this.park = item;
    this.parks = [];
    this.search = "";
    this.carSelection();
  }

  

  cancelParkSelection() {
    this.park = null;
    this.vehicleSelected = null;
    this.properties = [];
    this.property = null;
  }

  async carSelection() {
    this.vehicleSelected = null;

    const popover = await this.popoverController.create({
      component: VehicleSelectionPage,
      translucent: true,
      cssClass: 'popover'
    });

    await popover.present();

    popover.onDidDismiss().then(x => {
      this.vehicleSelected = x.data;

    });

  }

  onSearchProperty() {

    var temp = this.searchProperty;

    setTimeout(() => {
      if (temp == this.searchProperty) {
        this.propertyService.SearchByCommunity(this.searchProperty, this.park.community.id).subscribe(x => {
          this.properties = x;
        });
      }
    }, 500);


  }
  onSelectProperty(item: PropertyModel) {
    this.searchProperty = "";
    this.property = item;
    this.properties = [];
  }

  removeProperty() {
    this.property = null;
  }

  async onPark() {
    var visit = new VisitModel();
    visit.parkingId = this.park.id;
    visit.propertyId = this.property.id;
    visit.vehicleId = this.vehicleSelected.id;

    await this.utils.presentLoading();
    this.visitService.park(visit).subscribe(x => {
      this.visitService.getMyLastPark(this.visitorService.visitor.id).subscribe(x => {
        this.utils.dismissLoading();
        this.cancelParkSelection();
        if (this.visitService.visit != null && !this.visitService.visit.wasCancelled && !this.visitService.visit.hasExpired) {
          setTimeout(() => {
            this.resetCounter(this.visitService.visit.seconds);
          }, 500);
        }
      }, () => { this.utils.dismissLoading(); });
    }, () => { this.utils.dismissLoading(); });
  }

  async onCancelPark() {
    this.utils.presentAlert([
      {
        text: 'Yes', handler: async () => {
          await this.utils.presentLoading();
          this.visitService.cancelVisit().subscribe(x => {
            this.utils.dismissLoading();
          });
        },
        cssClass :'alertButton'
      },
      {
        text: 'No', handler: async () => {
          
        },
        cssClass: 'alertButton'
        
      }
      
    ], 'Cancel Parking', '', 'Are you sure?');



  }

}
