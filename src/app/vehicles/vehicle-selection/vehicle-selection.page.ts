import { Component, OnInit } from '@angular/core';
import { UtilsService } from 'src/app/services/utils.service';
import { VehicleService } from 'src/app/services/vehicle.service';
import { VisitorService } from 'src/app/services/visitor.service';
import { PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vehicle-selection',
  templateUrl: './vehicle-selection.page.html',
  styleUrls: ['./vehicle-selection.page.scss'],
})
export class VehicleSelectionPage implements OnInit {

  constructor(
    public utils: UtilsService,
    public vehicleService: VehicleService,
    public visitorService: VisitorService,
    private popoverCOntroller: PopoverController,
    private router:Router
  ) { }

  ngOnInit() {
    // this.getListVehicles();
  }

  // async getListVehicles() {
  //   await this.utils.presentLoading();
  //   this.vehicleService.GetListByVisitor(this.visitorService.visitor.id).subscribe(x => {
  //     this.utils.dismissLoading();
  //   }, () => { this.utils.dismissLoading(); });
  // }

  select(item){
    this.popoverCOntroller.dismiss(item);
  }

  addSelect(){
    this.router.navigate(['vehicle','Add','Parking'])
    this.select(null);
  }
  
}
