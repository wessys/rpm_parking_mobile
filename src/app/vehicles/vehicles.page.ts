import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { VisitorService } from '../services/visitor.service';
import { Router } from '@angular/router';
import { VehicleService, VehicleModel } from '../services/vehicle.service';
import { UtilsService } from '../services/utils.service';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.page.html',
  styleUrls: ['./vehicles.page.scss'],
})
export class VehiclesPage implements OnInit {

  constructor(
    public actionSheetController: ActionSheetController,
    private visitorService: VisitorService,
    public router: Router,
    public vehicleService: VehicleService,
    public utils: UtilsService
  ) { }

  ngOnInit() {

  }

  ionViewWillEnter() {
    if(this.visitorService.visitor.id){
      this.getListVehicles();
    }
    else{
      setTimeout(() => {
        this.getListVehicles();
      }, 1000);
    }
  }


  async getListVehicles(){
    await this.utils.presentLoading();
    this.vehicleService.GetListByVisitor(this.visitorService.visitor.id).subscribe(x => {
      this.utils.dismissLoading();
    }, () => { this.utils.dismissLoading(); });
  }

  async showPicker(vehicle: VehicleModel) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Options',
      cssClass:'action',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.utils.presentAlert([{text:'Cancel', cssClass:'alertButton'},{text:'Ok', cssClass:'alertButton',handler: async ()=>{
            
            await this.utils.presentLoading();
            this.vehicleService.Delete(vehicle.id).subscribe(x=>{
              this.utils.dismissLoading();
              this.getListVehicles();
            },()=>{
              this.utils.dismissLoading();
            });


          }}],'Delete','','Are you sure?', "alertCss")


        }
      },
      {
        text: 'Update',
        role: 'update',
        icon: 'ios-hammer',
        handler: () => {
          this.vehicleService.vehicle = vehicle;
          this.router.navigate(['vehicle', 'Update','Vehicles']);
        }
      }
        , {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {

        }
      }]
    });
    await actionSheet.present();


  }

  addVehicle() {
    this.router.navigate(['vehicle', 'Add','Vehicles']);
  }

}
