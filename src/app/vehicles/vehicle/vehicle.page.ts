import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Routes, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { VehicleService, VehicleModel } from 'src/app/services/vehicle.service';
import { VisitorService } from 'src/app/services/visitor.service';
import { UtilsService } from 'src/app/services/utils.service';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.page.html',
  styleUrls: ['./vehicle.page.scss'],
})
export class VehiclePage implements OnInit {

  action: string = "";
  from: string = "";

  vehicleForm = this.fb.group({
    nick: this.fb.control('', [Validators.required]),
    tag: this.fb.control('', [Validators.required]),
    vehicleType: this.fb.control('Car', [Validators.required]),
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private vehicleService: VehicleService,
    private visitorService: VisitorService,
    public utils: UtilsService,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.route.params.subscribe(x => {
      this.action = x.action;
      this.from = x.from;
      console.log(this.from);
      if (this.action == "Add") {
        this.vehicleService.vehicle = new VehicleModel();
        this.vehicleService.vehicle.visitorId = this.visitorService.visitor.id;
      }
      else if (this.action == "Update") {
        this.vehicleForm.reset();
        this.vehicleForm.setValue({
          tag: this.vehicleService.vehicle.tag,
          nick: this.vehicleService.vehicle.nick,
          vehicleType: this.vehicleService.vehicle.vehicleType
        });
      }
    })
  }

  async save() {

    this.vehicleService.vehicle.tag = this.vehicleForm.controls.tag.value;
    this.vehicleService.vehicle.nick = this.vehicleForm.controls.nick.value;
    this.vehicleService.vehicle.vehicleType = this.vehicleForm.controls.vehicleType.value;

    await this.utils.presentLoading();
    this.vehicleService.Save().subscribe(x => {

      this.utils.dismissLoading();
      
      if (!x.isSuccess) {
        this.utils.presentToast(x.reason, 'danger', 4000);
      }
      else {
        this.getListVehicles();
        if (this.from == 'Vehicles') {
          this.router.navigate(['menu', 'vehicles']);
        }
        else {
          this.router.navigate(['menu', 'park']);
        }
      }




    }, () => {
      this.utils.dismissLoading();
    });
  }

  getListVehicles() {

    this.vehicleService.GetListByVisitor(this.visitorService.visitor.id).subscribe();
  }

  async onClickType() {

    const actionSheet = await this.actionSheetController.create({
      header: 'Vehicle type',
      cssClass: 'action',
      buttons: [{
        text: 'Car',
        role: 'destructive',
        icon: 'ios-car',
        handler: () => {
          this.vehicleForm.controls.vehicleType.setValue('Car');
        }
      },
      {
        text: 'Motorcycle',
        role: 'update',
        icon: '../../assets/images/motor.svg',
        handler: () => {
          this.vehicleForm.controls.vehicleType.setValue('Motorcycle');

        }
      },
      {
        text: 'Truck',
        role: 'update',
        icon: '../../assets/images/truck.svg',
        handler: () => {
          this.vehicleForm.controls.vehicleType.setValue('Truck');
        }
      }]
    });
    await actionSheet.present();


  }




}
