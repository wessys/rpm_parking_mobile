import { Component, OnInit } from '@angular/core';
import { ParkService } from '../services/park.service';
import { VisitModel } from '../services/visitor.service';
import { AlertController } from '@ionic/angular';
import { UtilsService } from '../services/utils.service';

@Component({
  selector: 'app-inspection',
  templateUrl: './inspection.page.html',
  styleUrls: ['./inspection.page.scss'],
})
export class InspectionPage implements OnInit {

  visits: VisitModel[] = null;
  visit: VisitModel = null;


  locationNumber: string = "";
  licensePlate: string = "";

  noResults: boolean = false;

  constructor(
    public service: ParkService, 
    public alertController: AlertController,
    public utils : UtilsService
    ) { }

  ngOnInit() {
    // this.onSearch();
    if (this.service.mode == null)
      this.service.mode = "plate";
  }

  async onSearch() {
    this.noResults = false;
    await this.utils.presentLoading();
    if (this.service.mode == "location") {
      this.service.CheckPermitByParkingNumber(this.locationNumber).subscribe(x => {
        this.utils.dismissLoading();
        this.visits = x;

        if(x == null){
          this.noResults = true;
        }
        else{
          if (this.visits.length == 0)
          this.noResults = true;
        }


        
      



        },()=>{this.utils.dismissLoading();});
    }
    else if (this.service.mode == "plate") {
      this.service.CheckPermitByPlate(this.locationNumber, this.licensePlate).subscribe(x => {
        this.utils.dismissLoading();
        this.visit = x.model;
        
        if(!x.isSuccess){
          this.noResults = true;
          this.utils.presentToast(x.reason,'danger',4000);
        }
          

        console.log(x)
      },()=>{this.utils.dismissLoading();});
    }

  }


  onChange() {
    this.visits = null;
    this.visit = null;
    this.noResults = false;
  }



  async onSelectMode() {
    const alert = await this.alertController.create({
      header: 'Search Mode',
      cssClass: 'radio-alertCss',
      inputs: [
        {
          name: 'By Location',
          type: 'radio',
          label: 'By Location',
          value: 'location',
          checked: this.service.mode == 'location'
        },
        {
          name: 'By License Plate',
          type: 'radio',
          label: 'By License Plate',
          value: 'plate',
          checked: this.service.mode == 'plate'
        },

      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'alertButton',
          handler: () => {

          }
        }, {
          text: 'Ok',
          handler: (x) => {
            this.noResults =false;
            this.service.mode = x;
            this.visits = null;
            this.visit = null;
            this.locationNumber = "";
            this.licensePlate = "";
          }
          , cssClass: 'alertButton'
        }
      ]
    });

    await alert.present();
  }


}
