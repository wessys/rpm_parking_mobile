import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UrlService } from './url.service';
import { Observable } from 'rxjs';
import { PropertyModel } from './park.service';
import { map, timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {

  constructor(private http: HttpClient, private urlService: UrlService) { }

  SearchByCommunity(search: string, communityId:number): Observable<PropertyModel[]> {
    return this.http.get(`${this.urlService.api.property}/SearchByCommunity?take=5&skip=0&communityId=${communityId}&search=${search}`).pipe(map((res: any) => res as PropertyModel[]));
  }
}
