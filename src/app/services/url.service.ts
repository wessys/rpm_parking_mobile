import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlService {

  //base: string = "http://10.1.10.239:53349/";
  timeout: number = 10000;
  base: string = "http://20.115.28.47/";
  //base: string = "http://192.168.100.8:7081/";

  api: {
    security,
    visitor,
    parking,
    visit,
    vehicle,
    owner,
    community,
    property

  }

  constructor() {
    this.generateurl();
  }

  generateurl() {
    this.api = {
      security: this.base + 'api/security',
      visitor: this.base + 'api/visitor',
      parking: this.base + 'api/parking',
      visit: this.base + 'api/visit',
      vehicle: this.base + 'api/vehicle',
      owner: this.base + 'api/owner',
      community: this.base + 'api/community',
      property: this.base + 'api/property',
    }
  }

}
