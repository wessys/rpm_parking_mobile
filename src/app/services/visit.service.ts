import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UrlService } from './url.service';
import { Observable } from 'rxjs';
import { VisitModel } from './visitor.service';
import { map, tap, timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VisitService {

  visit: VisitModel;

  constructor(private http: HttpClient, private urlService: UrlService) { }

  park(model: VisitModel): Observable<any> {
    return this.http.post(`${this.urlService.api.visit}/add`, model).pipe(map((res: any) => res as any));
  }

  getMyLastPark(visitorId: number): Observable<VisitModel> {
    return this.http.get(`${this.urlService.api.visit}/GetMyLastPark/${visitorId}`).pipe(map((res: any) => res as VisitModel), tap(x => { this.visit = x }));
  }

  cancelVisit(): Observable<any> {
    return this.http.get(`${this.urlService.api.visit}/CancelVisit/${this.visit.id}`).pipe(map((res: any) => res as any), tap(x => {
      this.visit = null;
    }));
  }

  

  
}
