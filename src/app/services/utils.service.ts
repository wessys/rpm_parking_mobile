import { Injectable } from '@angular/core';
import { ToastController, AlertController, LoadingController } from '@ionic/angular';
import { AlertButton } from '@ionic/core';


@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  private loading: any;

  constructor(
    private toastController: ToastController,
    private alertController: AlertController,
    private loadingCtrl: LoadingController
  ) {


  }

  // "primary", "secondary", "tertiary", "success", "warning", "danger", "light", "medium", and "dark"
  async presentToast(message: string, color?: string, duration?: number) {
    const toast = await this.toastController.create({
      message: message,
      duration: duration ? duration : 2000,
      color: color ? color : "success",
      position: "bottom",
    });
    toast.present();
  }

  async presentErrorToast(message: string, duration?: number) {
    const toast = await this.toastController.create({
      message: message,
      duration: duration ? duration : 10000,
      showCloseButton:true,
      color: "danger",
      position: "bottom"
    });
    toast.present();
  }

  async presentAlert(buttons: AlertButton[], header: string, subHeader: string, message: string , css?:string) {
    const alert = await this.alertController.create({
      header: header,
      subHeader: subHeader,
      message: message,
      buttons: buttons,
      cssClass: css ? css : 'alertCss'

    });
    return await alert.present();
  }

  presentLoading() : Promise<any> {

    return new Promise<any>(resolve => {
      this.loadingCtrl.create({
        message : 'Please Wait ...',
        cssClass: 'loading'
      }).then(x=>{
        this.loading = x;
        this.loading.present();
        resolve();
      }); 
  });

   

    

  }
  dismissLoading() {
    try{
      this.loading.dismiss();
    }
    catch(e){
      setTimeout(() => {
        this.loading.dismiss();
      }, 1000);
    }
    
  }



}


