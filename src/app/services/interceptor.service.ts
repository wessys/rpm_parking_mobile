import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { filter, take, switchMap, catchError, timeout } from 'rxjs/operators';
import { LoginService, Tokens } from './login.service';
import { Router } from '@angular/router';
import { UtilsService } from './utils.service';

@Injectable({
  providedIn: 'root'
})

export class AuthInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService, private loginService: LoginService, private router:Router , private utils:UtilsService) { }

  // intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

  //   const authHeader = this.auth.GetAuthorization();

  //   req = req.clone({
  //     setHeaders: {
  //       Authorization: authHeader
  //     }
  //   })

  //   return next.handle(req);
  // }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if ( request.url.indexOf('login') == -1) {
      request = this.addToken(request);
    }
    else{
      this.isRefreshing = false;
    }
    
    
    return next.handle(request).pipe( timeout(10000), catchError(error => {
     
      if (error instanceof HttpErrorResponse && error.status === 401) {
        return this.handle401Error(request, next);
      }
      else if(request.url.indexOf('refresh-token') >= 0 ){
        this.auth.RemoveUser();
        this.utils.dismissLoading();
        this.router.navigate(['register-or-login']);
      }
      else {
        this.utils.dismissLoading();
        this.utils.presentErrorToast('There was a server Error. Please try again.');
        return throwError(error);
      }
    }));
  }

  private addToken(request: HttpRequest<any>) {
    const authHeader = this.auth.GetAuthorization();

    request = request.clone({
      setHeaders: {
        Authorization: authHeader
      }
    })
    return request;
  }

  private isRefreshing = false;

  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {

    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);

      return this.loginService.refreshToken().pipe(
        switchMap((token: Tokens) => {
          this.isRefreshing = false;
          this.refreshTokenSubject.next(token.token);
          return next.handle(this.addToken(request));
        }));

    } else {
      return this.refreshTokenSubject.pipe(
        filter(token => token != null),
        take(1),
        switchMap(jwt => {
          return next.handle(this.addToken(request));
        }));
    }
  }

  
} 
