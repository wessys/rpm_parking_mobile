import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  private readonly JWT_TOKEN = 'token';
  private readonly REFRESH_TOKEN = 'refresh_token';
  
  role: 'visitor' | 'employee' | 'owner' | 'Admin' | 'compliance';

  user: User;
  constructor( private router:Router) { }


  GoToRightView(){
    var user = this.GetUser() ;
      
      if(user != null)
      {
        if(user.roleName == 'compliance' ){
          this.router.navigate(['menu','inspection']);
        }
        else {
          this.router.navigate(['menu','park']);
        }
      }
      
  }

  IsAuthorized(): boolean {
    let _user: User = JSON.parse(localStorage.getItem("user"));
    if (_user != null) {
      return true;
    }
    else {

      return false;
    }
  }

  SetUser(_user: User): void {
    this.user = _user;
    localStorage.setItem("user", JSON.stringify(_user));
  }

  GetUser(): User {
    let _user: User = JSON.parse(localStorage.getItem("user"));
    return _user;
  }

  RemoveUser(): void {
    localStorage.removeItem("user");
  }


  GetAuthorization(): string {
    if (this.IsAuthorized()) {
      return "Bearer " + this.GetUser().token;
    }
    else {
      return "";
    }
  }


  getJwtToken() {
    return this.GetUser().token;
  }


  public getRefreshToken() :string{
    return this.GetUser().refresh_token;
  }

  public storeJwtToken(jwt: string) {

    var user = this.GetUser();

    user.token = jwt;

    this.SetUser(user);

  }
  public storeJwtRefreshToken(refreshToken: string) {

    var user = this.GetUser();

    user.refresh_token = refreshToken;

    this.SetUser(user);

  }

}

export class User {
  public id: string;
  public firstName: string;
  public lastName: string;
  public phoneNumber: string;
  public email: string;
  public token: string;
  public refresh_token: string;
  public expiration: Date;
  public roleName: string ;
  public password: string;
  public userName: string;
  public fullName: string;
  public code:string;
  public isOwner:boolean;
  public account:string;


}
