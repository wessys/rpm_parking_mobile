import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UrlService } from './url.service';
import { Observable } from 'rxjs';
import { map, tap, timeout } from 'rxjs/operators';
import { PropertyModel } from './park.service';

@Injectable({
  providedIn: 'root'
})
export class OwnerService {

  owner: OwnerModel;

  constructor(private http: HttpClient, private urlService: UrlService) { }

  GetVisitorLogged(): Observable<OwnerModel> {
    return this.http.get(`${this.urlService.api.owner}/getbylogged`).pipe(map((res: any) => res as OwnerModel),tap(x=>{this.owner = x;}) );
  }
}


export class OwnerModel {
  id?: number;
  name?: string;
  phoneNumber?: string;
  email?: string;
  userId?: string;

  propertyOwner?: PropertyOwnerModel[];
}

export class PropertyOwnerModel {
  id?: number;
  propertyId?: number;
  ownerId?: number;

  owner?: OwnerModel;
  property?: PropertyModel;
}

