import { Injectable } from '@angular/core';
import { VisitModel, VisitorModel } from './visitor.service';
import { PropertyOwnerModel } from './owner.service';
import { HttpClient } from '@angular/common/http';
import { UrlService } from './url.service';
import { Observable } from 'rxjs';
import { map, timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ParkService {

  mode: "plate" | "location";

  constructor(private http: HttpClient, private urlService: UrlService) { }

  Search(search: string): Observable<ParkingModel[]> {
    return this.http.get(`${this.urlService.api.parking}/getallpaged?take=5&skip=0&search=${search}`).pipe(map((res: any) => res as ParkingModel[]));
  }

  SearchByNumber(search: string): Observable<ParkingModel> {
    return this.http.get(`${this.urlService.api.parking}/searchByNumber?search=${search}`).pipe(map((res: any) => res as ParkingModel));
  }

  CheckPermitByParkingNumber(parkingNumber: string): Observable<VisitModel[]> {
    return this.http.get(`${this.urlService.api.parking}/check-permit-by-parking-number/${parkingNumber}`).pipe(map((res: any) => res as VisitModel[]));
  }

  CheckPermitByPlate(parkingNumber: string,plate: string): Observable<any> {
    return this.http.get(`${this.urlService.api.parking}/check-permit-by-plate/${parkingNumber}/${plate}`).pipe(map((res: any) => res as any));
  }

}



export class ParkingModel {
  id?: number;
  number?: string;
  communityId?: number;
  latitude?: string;
  longitude?: string;
  inactive?: boolean;

  community?: CommunityModel;
  visit?: VisitModel[];
}

export class CommunityModel {
  id?: number;
  name?: string
  address?: string
  communityNumber?: string;
  visitTypeId?: number;
  visitValue?: number;
  inactive?: boolean;


  parking?: ParkingModel[];
  property?: PropertyModel[];
}

export class PropertyModel {
  id?: number;
  address?: string;
  accountNumber?: number;
  communityId?: number;
  visitValue?: number;
  inactive?: boolean;

  community?: CommunityModel;
  visit?: VisitorModel[];
  propertyOwner?: PropertyOwnerModel[];
}
