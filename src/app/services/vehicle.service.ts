import { Injectable } from '@angular/core';
import { VisitorModel, VisitModel } from './visitor.service';
import { UrlService } from './url.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap, timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  vehicle: VehicleModel;

  vehicles: VehicleModel[];

  constructor(private http: HttpClient, private urlService: UrlService) { }

  GetListByVisitor(visitorId: number): Observable<VehicleModel[]> {
    return this.http.get(`${this.urlService.api.vehicle}/getallbyvisitor/${visitorId}`).pipe(map((res: any) => res as VehicleModel[]), tap(x => { this.vehicles = x; }));
  }

  Save(): Observable<any> {
    if (this.vehicle.id > 0) {
      return this.Update()
    }
    else {
      return this.Add();
    }
  }

  private Update(): Observable<any> {
    return this.http.put(`${this.urlService.api.vehicle}/update`, this.vehicle).pipe(map((res: any) => res as any), tap(x => { this.vehicle = x; }));
  }

  private Add(): Observable<any> {
    return this.http.post(`${this.urlService.api.vehicle}/add`, this.vehicle).pipe(map((res: any) => res as any), tap(x => { this.vehicle = x; }));
  }

  Delete(id: number): Observable<boolean> {
    return this.http.delete(`${this.urlService.api.vehicle}/delete/${id}`).pipe(map((res: any) => res as boolean));
  }


}



export class VehicleModel {
  id?: number;
  tag?: string;
  nick?: string;
  visitorId?: number;
  inactive?: boolean;
  vehicleType?:string;
  visitor?: VisitorModel;

  visit?: VisitModel[];
}