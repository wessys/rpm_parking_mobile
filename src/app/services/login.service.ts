import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { UrlService } from './url.service';
import { map, tap, timeout, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  loggedAs : 'employee' | 'visitor'; 
  user: User = new User();

  constructor(private http: HttpClient, private urlService: UrlService, private authService: AuthService) { }

  GetToken(_user: User): Observable<any> {
    return this.http.post(`${this.urlService.api.security}/login`, _user).pipe(map((res: any) => res as any));
  }

  RegisterFromMobile(user: User): Observable<any> {
    return this.http.post(`${this.urlService.api.security}/register-from-mobile`, user).pipe(map((res: any) => res as any));
  }

  CheckMobileCode(user: User): Observable<any> {
    return this.http.post(`${this.urlService.api.security}/check-mobile-code`, user).pipe(map((res: any) => res as any));
  }

  MobileGenerateCode(phone: string): Observable<any> {
    return this.http.get(`${this.urlService.api.security}/mobile-generate-code/${phone}`).pipe(map((res: any) => res as any));
  }

  ChangePasswordFromMobile(user: User): Observable<any> {
    return this.http.post(`${this.urlService.api.security}/changepassword-from-mobile`, user).pipe(map((res: any) => res as any));
  }

  refreshToken() {
    var tokensOld = new Tokens();
    tokensOld.token = this.authService.getJwtToken();
    tokensOld.refresh_token =  this.authService.getRefreshToken();
    console.log(tokensOld);

    return this.http.post<any>(`${this.urlService.api.security}/refresh-token`,tokensOld)
    .pipe(
      tap((tokens: Tokens) => {
      this.authService.storeJwtToken(tokens.token);
      this.authService.storeJwtRefreshToken(tokens.refresh_token);
    }));
  }


}

export class Tokens{
  token: string;
  refresh_token:string;
}
