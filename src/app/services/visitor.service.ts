import { Injectable } from '@angular/core';
import { UrlService } from './url.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, tap, timeout } from 'rxjs/operators';
import { VehicleModel } from './vehicle.service';
import { ParkingModel, PropertyModel } from './park.service';

@Injectable({
  providedIn: 'root'
})
export class VisitorService {

  visitor: VisitorModel;

  constructor(private http: HttpClient, private urlService: UrlService) { }

  GetVisitorLogged(): Observable<any> {
    return this.http.get(`${this.urlService.api.visitor}/getbylogged`).pipe(map((res: any) => res as any), tap(x=>{this.visitor = x;}));
  }

}

export class VisitorModel {

  id?: number;
  phoneNumber?: string;
  userId?: string;
  inactive?: boolean;

}

export class VisitModel {
  id?: number;
  parkingId?: number;
  vehicleId?: number;
  createdDate?: Date;
  hasExpired?: boolean;
  propertyId?: number;
  wasAccepted?:boolean;
  wasCancelled?:boolean;
  dateCancelled?: Date;
  seconds?:number;
  
  parking?: ParkingModel;
  vehicle?: VehicleModel;
  property?: PropertyModel;

}
