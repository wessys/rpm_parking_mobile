import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { RouteRedirect } from '@ionic/core';
import { AuthService } from '../services/auth.service';
import { OwnerService } from '../services/owner.service';
import { VisitorService } from '../services/visitor.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  selectedPath = '';
  allpages = [

    {
      title: 'Park',
      url: '/menu/park',
      icon: 'ios-navigate',
      roles : ['visitor', 'owner']
    },
    
    {
      title: 'Vehicles',
      url: '/menu/vehicles',
      icon: 'ios-car',
      roles : ['visitor', 'owner']
    },
    {
      title: 'Compliance',
      url: '/menu/inspection',
      icon: 'ios-clipboard',
      roles : ['compliance']

    }
    
    // {
    //   title: 'Notifications',
    //   url: '/menu/notifications',
    //   icon: 'ios-notifications'
    // }

  ];

  pages = [];

  constructor(
    private router: Router, 
    private auth: AuthService, 
    private ownerService: OwnerService, 
    private visitorService: VisitorService) { }

  ngOnInit() {
    
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectedPath = event.url;
    });
    
    this.ownerService.GetVisitorLogged().subscribe();
    
    var user = this.auth.GetUser();
    
    this.allpages.forEach(page => {
      
      if(page.roles.find(x=> x == user.roleName )){
        
        this.pages.push(page);
      }

    });


  }

  logOff() {
    this.auth.RemoveUser();
    this.router.navigate(['register-or-login'])
  }

}
