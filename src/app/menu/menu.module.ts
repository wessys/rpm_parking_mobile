import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MenuPage } from './menu.page';



const routes: Routes = [
  { path: 'menu', redirectTo: '/menu/park', pathMatch:'full' },
  {
    path: 'menu', component: MenuPage, children: [
      { path: 'park', loadChildren: '../park/park.module#ParkPageModule' },
      { path: 'notifications',loadChildren: '../notifications/notifications.module#NotificationsPageModule' },
      { path: 'vehicles',loadChildren: '../vehicles/vehicles.module#VehiclesPageModule' },
      { path: 'inspection', loadChildren: '../inspection/inspection.module#InspectionPageModule' }
    ]
  }
  
 
]

@NgModule({ 
  imports: [ 
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
    

  ],
  exports:[RouterModule] 
  ,
  declarations: [MenuPage]
})
export class MenuPageModule {}
