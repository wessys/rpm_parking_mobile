import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'register-or-login', pathMatch: 'full' },
  { path: 'login', loadChildren: './authPages/login/login.module#LoginPageModule' },
  { path: 'forgot-password', loadChildren: './authPages/forgot-password/forgot-password.module#ForgotPasswordPageModule' },
  { path: 'code-verification/:type', loadChildren: './authPages/code-verification/code-verification.module#CodeVerificationPageModule' },
  { path: 'new-password', loadChildren: './authPages/new-password/new-password.module#NewPasswordPageModule' },
  { path: 'register/:type', loadChildren: './authPages/register/register.module#RegisterPageModule' },
  { path: 'register-or-login', loadChildren: './authPages/register-or-login/register-or-login.module#RegisterOrLoginPageModule' },
  { path: 'vehicle/:action/:from', loadChildren: './vehicles/vehicle/vehicle.module#VehiclePageModule' },

  

  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
 