import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/services/auth.service';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-code-verification',
  templateUrl: './code-verification.page.html',
  styleUrls: ['./code-verification.page.scss'],
})
export class CodeVerificationPage implements OnInit {

  errorMessage: string = null;
  type: string = "";

  userForm = this.fb.group({
    code: this.fb.control('', [Validators.required, Validators.max(99999999), Validators.min(10000000)]),
  });


  constructor(public login: LoginService, private router: Router, private utils: UtilsService, private fb: FormBuilder, private route: ActivatedRoute) { }

  ngOnInit() {

  }

  ionViewWillEnter() {

    this.userForm.reset();
    this.type = this.route.snapshot.paramMap.get("type");

  }


  onKey(event) {
    this.errorMessage = null;
  }

  async checkCode() {
    this.errorMessage = null;
    this.login.user.code = this.userForm.value.code;

    await this.utils.presentLoading();
    this.login.CheckMobileCode(this.login.user).subscribe(result => {
      this.utils.dismissLoading();
      if (result.isSuccess == true) {
        if (this.type == 'registration') {
          this.router.navigate(['login']);
          this.utils.presentToast('The account was registrated correctly. Please intruduce the credentials.', 'success', 7000);
        }
        else {
          this.router.navigate(['new-password']);
        }
      }
      else {
        this.errorMessage = result.reason;
      }
    }, () => {
      this.utils.dismissLoading();
      this.errorMessage = "There was an error";
    });

  }

}
