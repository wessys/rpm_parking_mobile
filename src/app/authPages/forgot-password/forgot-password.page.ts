import { Component, OnInit } from '@angular/core';
import { User } from '../../services/auth.service';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import { UtilsService } from 'src/app/services/utils.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  errorMessage: string = null;
  userForm = this.fb.group({
    userName: this.fb.control('', [Validators.required]),
  });

  constructor(public login: LoginService, private router: Router, private utils: UtilsService, private fb: FormBuilder) { }

  ngOnInit() {
  }

  async checkPhone() {
    this.errorMessage = null;
    await this.utils.presentLoading();
    this.login.MobileGenerateCode(this.userForm.value.userName).subscribe(x => {
      this.utils.dismissLoading();
      if (x == true) {
        this.login.user = new User();
        this.login.user.userName = this.userForm.value.userName;
        this.utils.presentToast('Please check your messages and enter the code sent.', "success", 4000);
        this.router.navigate(['code-verification','forgot-password']);
      }
      else {
        this.errorMessage = "*There is not an user registered in our system with this phone number. Please check it.";
        // this.utils.presentToast('Phone not recognized.', "danger", 4000);
      }
    }, () => {
      this.utils.dismissLoading();
      this.errorMessage = "*There was a server error."; 
      // this.utils.presentToast('There was a server error.', "danger", 4000);
    })
  }

  goToLogin() {
    this.router.navigate(['login'])
  }



}
