import { Component, OnInit } from '@angular/core';
import { UtilsService } from 'src/app/services/utils.service';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-register-or-login',
  templateUrl: './register-or-login.page.html',
  styleUrls: ['./register-or-login.page.scss'],
})
export class RegisterOrLoginPage implements OnInit {

  constructor(public utils: UtilsService, private router: Router, private loginService: LoginService) { }

  ngOnInit() {
  }

  register() {
    this.utils.presentAlert([
      {
        text: "Visitor", cssClass: 'alertButton', handler: () => {

          this.router.navigate(['register', 'visitor']);
        }
      },
      {
        text: "Owner", cssClass: 'alertButton', handler: () => {
          this.router.navigate(['register', 'owner']);
        }
      }

    ], "Account holder", "", "", "alertCss");
  }

  login() {
    this.router.navigate(['login']);
  }



}
