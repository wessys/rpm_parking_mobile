import { Component, OnInit } from '@angular/core';
import { User, AuthService } from 'src/app/services/auth.service';
import { LoginService } from 'src/app/services/login.service';
import { Router } from '@angular/router';
import { UtilsService } from 'src/app/services/utils.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.page.html',
  styleUrls: ['./new-password.page.scss'],
})
export class NewPasswordPage implements OnInit {

  errorMessage: string[] = [];

  userForm = this.fb.group({
    password: this.fb.control('', [Validators.required, Validators.minLength(6)]),
  });

  constructor(
    public service: LoginService,
    private authService: AuthService,
    private router: Router,
    private utils: UtilsService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {

  }

  onKey(event){
    this.errorMessage = [];
  }
  async newPassword() {

    this.errorMessage = [];

    await this.utils.presentLoading();

    this.service.user.password = this.userForm.value.password;

    this.service.ChangePasswordFromMobile(this.service.user).subscribe(x => {
      if (x.succeeded) {
        this.utils.presentToast('Password changed successfully','success');
        this.router.navigate(['login'])
      }
      else {
        x.errors.forEach(element => {
          this.errorMessage.push(element.description);
        });

      }
      this.utils.dismissLoading();
    }, () => {
      this.errorMessage.push('There was a server error')
      this.utils.dismissLoading();
    });

  }

}
