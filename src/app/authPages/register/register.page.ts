import { Component, OnInit } from '@angular/core';
import { User, AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { UtilsService } from 'src/app/services/utils.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  user: User = new User();

  userForm = this.fb.group({
    userName: this.fb.control('', [Validators.required, Validators.maxLength(12)]),
    password: this.fb.control('', [Validators.required, Validators.minLength(6)]),
    email: this.fb.control('', [Validators.required, Validators.email]),
    account: this.fb.control(''),
    fullName: this.fb.control('')
  });

  constructor(
    private route: ActivatedRoute,
    private service: LoginService,
    private authService: AuthService,
    private router: Router,
    private utils: UtilsService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    


  }


  setValidators() {
    let fullNameControl = this.userForm.get('fullName');
    
    if(!this.user.isOwner){
      fullNameControl.setValidators([Validators.required]);
    }
    
  }



  ionViewWillEnter() { 

    this.userForm.reset();
    let type = this.route.snapshot.paramMap.get("type");
    type == 'owner' ? this.user.isOwner = true : this.user.isOwner = false;
    this.setValidators();
    //this.credentialsError = false;
  }

  async register() {

    await this.utils.presentLoading();

    this.user.userName = this.userForm.value.userName;
    this.user.password = this.userForm.value.password;
    this.user.email = this.userForm.value.email;
    this.user.fullName = this.userForm.value.fullName;
    this.user.account = this.userForm.value.account;
    
    this.service.RegisterFromMobile(this.user).subscribe(x => {
      this.utils.dismissLoading();
      
      if (x.succeeded == true) {
        this.service.user = new User();
        this.service.user.userName = this.user.userName;

        this.router.navigate(['code-verification','registration']);
      }
      else {
        var msg = "";
        


        x.errors.forEach(error => {
          if(typeof(error) == "string"){
            msg = error;
          }
          else{
            if(error.code == "DuplicateUserName"){
              msg = `Phone number '${this.user.userName}' is already taken.`;
              return;
            }
            else{
              msg = error.description;
            }
          }
          
        });

        this.utils.presentErrorToast(msg);

      }

    }, () => {
      this.utils.dismissLoading();
    });

  }

  test() {
    console.log(this.userForm)
  }

}
