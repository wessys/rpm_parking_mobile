import { Component, OnInit } from '@angular/core';
import { User, AuthService } from '../../services/auth.service';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import { UtilsService } from 'src/app/services/utils.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  error: string = "";

  userForm = this.fb.group({
    userName: this.fb.control('', [Validators.required]),
    password: this.fb.control('', [Validators.required, Validators.minLength(6)]),
  });

  constructor(
    public service: LoginService,
    private authService: AuthService,
    private router: Router,
    private utils: UtilsService,
    private fb: FormBuilder
  ) { }

  ionViewWillEnter() {
    this.userForm.reset();
    this.error = null;
  }

  ngOnInit() {
    
  }

  onKey(event) {
    this.error = null;
  }

  async login() {
    this.error = null;
    await this.utils.presentLoading();
    this.service.GetToken(this.userForm.value).subscribe(x => {

      this.utils.dismissLoading();

      if (x.success == true) {
        this.authService.SetUser(x.model);
        this.authService.GoToRightView();
      }
      else {
        this.utils.presentErrorToast(x.reason);
        //this.error = x.reason;
      }


    }, (e) => {
      this.utils.presentErrorToast("There was a server error. Please try again.");
      // this.error = "There was a server error. Please try again.";
      this.utils.dismissLoading();

    });
  }

  forgotCredentials() {
    this.router.navigate(['/forgot-password']);

    

  }



}
