import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MenuPageModule } from './menu/menu.module';
import {HttpClient, HTTP_INTERCEPTORS, HttpClientModule  } from '@angular/common/http';
import { AuthInterceptor } from './services/interceptor.service';
import { ReactiveFormsModule } from '@angular/forms';
import { VehicleSelectionPageModule } from './vehicles/vehicle-selection/vehicle-selection.module';
import { CountdownModule } from 'ngx-countdown';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: 
  [BrowserModule, 
    IonicModule.forRoot({hardwareBackButton: false}),
    MenuPageModule , 
    AppRoutingModule, 
    VehicleSelectionPageModule ,  
    HttpClientModule,    
    ReactiveFormsModule, 
    CountdownModule  
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },

  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
